#include <iostream>
#include <stdlib.h>     //for system("cls");
#include <fstream>      //File support
#include <string>       //String support
#include <sstream>      //string, int conversion
#include <time.h>       //measuring time
#include <windows.h>
#include <cmath>        //round function
#include <regex>        //regex

using namespace std;
int choose =0;
int exit1234 = 0;
;
class FileStream
{
public:
    string time_tab[4];
    int time_tab_i[4];
    fstream file;
    string data;
    string temp;






    void load()
    {
        regex wzorzec( "Length  of program : (\\d{1,2})" );
        int i=0;
        data.clear();
        file.open( "Prog_Data.txt", ios::in );
        if( file.good() == true )
        {
            while(getline(file, temp))
            {
                data = data + "\n" + temp;
                //regex for finding time
                smatch wynik;
                if( regex_search( temp, wynik, wzorzec ) )
                {
                    time_tab[i] = wynik.str(1);
                    i++;
                }
            }
            for (i=0;i<4;i++)
                {
                    istringstream ss(time_tab[i]);
                    ss >> time_tab_i[i];
                }
        }
        else cout << "I couldn't load programs data :(" << endl;
        file.close();
    }

    void read_prog(int prog_num)
    {
        string prog_num_str;
        string end_str;
        int prog_num_1 = prog_num +1;
        stringstream ss;
        ss << prog_num;
        prog_num_str = ss.str();        //number of program (string)
        stringstream ss2;
        ss2 << prog_num_1;
        end_str = ss2.str();             //number of next program (ending, string)

        data.clear();
        fstream file;
        string temp;
        file.open( "Prog_Data.txt", ios::in );
        while(getline(file, temp))
        {
            if(temp==prog_num_str)
            {
                while(getline(file, temp) && temp!=end_str)
                {
                    data = data + "\n" + temp;
                }
            }
        }
    }
};

class Interface
{
public:
    int prog_num;
    int decision;
    FileStream f1;
    void display_start()
    {
        system("cls");
        f1.load();
        cout << "This is the Virtual Machine!" << endl;
        cout << "You can select from the following programs:" << endl;
        cout << f1.data << endl;
        cout << "---------------------------------------------" << endl;
        cout << "You can select the program by typing it's number or press (Q) to quit" << endl;
        while(true)
        {
            Sleep(10);
            if(GetKeyState('Q') & 0x8000)  {exit(0);}
            else if(GetKeyState('1') & 0x8000)
                {
                                  {prog_num = 1;break;}

                }

            else if(GetKeyState('2') & 0x8000)
                {

                                  {prog_num = 2;break;}

                }
            else if(GetKeyState('3') & 0x8000)
                {

                                  {prog_num = 3;break;}

                }
            else if(GetKeyState('4') & 0x8000)
                {

                                  {prog_num = 4;break;}

                }
            else continue;

        }
    }

    void prog_running()
    {
        clock_t start, end, pause_start, pause_end;
        double time_used = 0, pause_time = 0;
        int time_of_prog = f1.time_tab_i[prog_num-1];

        start = clock();
        while(time_used < time_of_prog)
        {
            pause_start = clock();
            if(GetKeyState('P') & 0x8000)
                {cout<<"Program stopped, press any key to continue\n";system("pause");}
            if(GetKeyState('S') & 0x8000)
                {break;}
            pause_end = clock();
            pause_time += (double)(pause_end - pause_start)/ CLOCKS_PER_SEC;
            end = clock();
            time_used = (double)(end - start)/ CLOCKS_PER_SEC - pause_time;

            system("cls");
            cout << "Program number "<< prog_num <<" is running"<< endl;
            cout<<"Time: "<<round(time_used)<<"min/"<<time_of_prog<<"min"<<"\n";
            cout << "To pause the program press (P) and to stop the program press (S)"<< endl;
            Sleep(50);
        }

        cout << "Program ended!" <<endl;
        system("pause");
    }


    void display_prog()
    {
        f1.read_prog(prog_num);
        system("cls");
        cout << "You selected program number "<< prog_num << endl;
        cout << f1.data <<endl;
        cout << "Do you want to start (Y/N)"<< endl<<endl;

        while((true) && (decision != 1) && (decision != 2) && (decision != 3) && (decision != 4))
        {
            Sleep(10);

            if(GetKeyState('Q') & 0x8000)  {exit(0);}
            else if(GetKeyState('Y') & 0x8000 )
                {
                                  {decision = 1;}

                }

            else if(GetKeyState('y') & 0x8000)
                {

                                  {decision = 1;}

                }
            else if(GetKeyState('N') & 0x8000)
                {

                                  {decision = 2;}

                }
            else if(GetKeyState('n') & 0x8000)
                {

                                  {decision = 2;}

                }
            else continue;
        }

        if (decision==1)
        {
                        cout<<"Ok - Lets make it"<<endl;
                        Sleep(2000);
                        decision=0;
                        Interface::prog_running();
        }
        if (decision==2)

            cout<<"Sorry - you should choose another program "<<endl;
            Sleep(2000);
            decision=0;
    }
};

int main()
{
    Interface i1;
    while(exit1234 != 1234)
    {
        i1.display_start();
        i1.display_prog();


        system("cls");
        cout << "Do you want to have another washing? "<< endl;
        cout << "If the answer is Yes:" << endl;
        cout << "Type: Y" << endl;
        cout << "If not:" << endl;
        cout << "Type: N" << endl;
        cout << "---------------------------------------------" << endl;
        cout << "You can select the program by typing it's number or press (Q) to quit" << endl;

            while((true) && (choose != 1) && (choose != 2) && (choose != 3) && (choose != 4))
            {

            Sleep(10);
            if(GetKeyState('Q') & 0x8000)  {exit(0);}
            else if(GetKeyState('n') & 0x8000)
                {
                                  {choose = 1;}

                }

            else if(GetKeyState('N') & 0x8000)
                {

                                  {choose = 1;}

                }
            else if(GetKeyState('y') & 0x8000)
                {

                                  {choose = 2;}

                }
            else if(GetKeyState('Y') & 0x8000)
                {

                                  {choose = 2;}

                }
            else continue;
            }


        if( choose == 1 )
        {
        choose = 0 ;
            exit1234 = 1234 ;
        }
        if( choose == 2 )
        {
            cout << "OK! - Lets do it again" << endl;
            Sleep(1000);
            choose = 0 ;
        }


    }

    return 0;
}

